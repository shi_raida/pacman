#include "pacman.h"
#include <iostream>


PacMan::PacMan(): MovingEntity() {
    this->loadSprites();
}

PacMan::PacMan(const unsigned int pos[2], const int dir[2], Map* map, std::string sprite): MovingEntity(pos, dir, map, sprite) {
    this->loadSprites();
}

void PacMan::loadSprites() {
    m_t_sprite = QPixmap("../PacMan/data/pacmanhaut.png");
    m_r_sprite = QPixmap("../PacMan/data/pacmandroit.png");
    m_b_sprite = QPixmap("../PacMan/data/pacmanbas.png");
    m_l_sprite = QPixmap("../PacMan/data/pacmangauche.png");

    if (m_t_sprite.isNull())
        throw std::runtime_error("Impossible to load 'pacmanhaut.png'.");
    if (m_r_sprite.isNull())
        throw std::runtime_error("Impossible to load 'pacmandroit.png'.");
    if (m_b_sprite.isNull())
        throw std::runtime_error("Impossible to load 'pacmanbas.png'.");
    if (m_l_sprite.isNull())
        throw std::runtime_error("Impossible to load 'pacmangauche.png'.");
}

void PacMan::setDir(const int dir[2]) {
    MovingEntity::setDir(dir);

    if (this->getHDir() == 1)
        this->loadSprite(m_b_sprite);
    if (this->getHDir() == -1)
        this->loadSprite(m_t_sprite);
    if (this->getVDir() == 1)
        this->loadSprite(m_r_sprite);
    if (this->getVDir() == -1)
        this->loadSprite(m_l_sprite);
}
