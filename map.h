#ifndef MAP_H
#define MAP_H

#include <QPixmap>

#define WALL '#'
#define WAY  '.'


class Map
{
public:
    // Constructors
    Map();
    Map(const std::string& field, const unsigned int size[2]);

    // Getters
    inline unsigned int* getSize() {return m_size;}
    inline unsigned int getWidth() {return m_size[0];}
    inline unsigned int getHeight() {return m_size[1];}
    inline std::string& getField() {return m_field;}
    char at(const unsigned int pos[2]);
    QPixmap& getSprite(unsigned int pos[2]);

    // Setters
    void setSize(const unsigned int size[2]);
    inline void setWidth(const unsigned int width) {m_size[0] = width;}
    inline void setHeight(const unsigned int height) {m_size[1] = height;}
    void setField(const std::string& field);

private:
    void loadSprites();


    unsigned int m_size[2];  // [width, height]
    std::string m_field;

    // Sprites
    QPixmap m_white_sprite;
    QPixmap m_tr_sprite;
    QPixmap m_tl_sprite;
    QPixmap m_br_sprite;
    QPixmap m_bl_sprite;
    QPixmap m_h_sprite;
    QPixmap m_v_sprite;
};

#endif // MAP_H
