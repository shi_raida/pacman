#include <stdexcept>
#include "map.h"

Map::Map() {
    std::string field = "####################"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "#..................#"
                        "####################";
    unsigned int size[2] = {15, 20};
    this->setSize(size);
    this->setField(field);
    this->loadSprites();
}

Map::Map(const std::string& field, const unsigned int size[2]) {
    this->setSize(size);
    this->setField(field);
    this->loadSprites();
}

void Map::setSize(const unsigned int size[2]) {
    this->setWidth(size[0]);
    this->setHeight(size[1]);
}

void Map::setField(const std::string& field) {
    char c;
    for (uint i = 0; i < this->getWidth() ; i++) {
        for (uint j = 0; j < this->getHeight() ; j++) {
            c = field[i*this->getHeight() + j];
            if (c != WALL && c != WAY)
                throw std::invalid_argument("Map::setField() : field contains characters other than WALL and WAY.");
        }
    }
    m_field = field;
}

char Map::at(const unsigned int pos[2]) {
    if (pos[0] >= this->getWidth())
        throw std::invalid_argument("Map::at() : pos_x must be in the map width.");
    else if (pos[1] >= this->getHeight())
        throw std::invalid_argument("Map::at() : pos_y must be in the map height.");
    else
        return m_field[pos[0]*this->getHeight() + pos[1]];
}

void Map::loadSprites() {
    m_white_sprite = QPixmap("../PacMan/data/Mur_blanc.png");
    m_br_sprite = QPixmap("../PacMan/data/Mur_coin_bas_droit.png");
    m_bl_sprite = QPixmap("../PacMan/data/Mur_coin_bas_gauche.png");
    m_tr_sprite = QPixmap("../PacMan/data/Mur_coin_haut_droit.png");
    m_tl_sprite = QPixmap("../PacMan/data/Mur_coin_haut_gauche.png");
    m_h_sprite = QPixmap("../PacMan/data/Mur_horizontal.png");
    m_v_sprite = QPixmap("../PacMan/data/Mur_vertical.png");

    if (m_white_sprite.isNull())
        throw std::runtime_error("Impossible to load 'Mur_blanc.png'");
    if (m_br_sprite.isNull())
        throw std::runtime_error("Impossible to load 'Mur_coin_bas_droit.png'");
    if (m_bl_sprite.isNull())
        throw std::runtime_error("Impossible to load 'Mur_coin_bas_gauche.png'");
    if (m_tr_sprite.isNull())
        throw std::runtime_error("Impossible to load 'Mur_coin_haut_droit.png'");
    if (m_tl_sprite.isNull())
        throw std::runtime_error("Impossible to load 'Mur_coin_haut_gauche.png'");
}

QPixmap& Map::getSprite(unsigned int pos[2]) {
    // Check if the pos is a correct argument and if it is a WALL or a WAY
    if (this->at(pos) == WAY) return m_white_sprite;

    // Wall
    unsigned int pb[2] = {pos[0]+1, pos[1]};
    unsigned int pr[2] = {pos[0], pos[1]+1};
    unsigned int pl[2] = {pos[0], pos[1]-1};
    unsigned int pt[2] = {pos[0]-1, pos[1]};
    unsigned int pbr[2] = {pos[0]+1, pos[1]+1};
    unsigned int pbl[2] = {pos[0]+1, pos[1]-1};
    unsigned int ptr[2] = {pos[0]-1, pos[1]+1};
    unsigned int ptl[2] = {pos[0]-1, pos[1]-1};

    bool b, r, l, t, br, bl, tr, tl;
    if (pos[0] > 0) {
        t = this->at(pt) == WALL;
        if (pos[1] > 0)
            tl = this->at(ptl) == WALL;
        if (pos[1] < this->getHeight()-1)
            tr = this->at(ptr) == WALL;
    } else {
        t = true;
        tl = true;
        tr = true;
    }
    if (pos[0] < this->getWidth()-1) {
        b = this->at(pb) == WALL;
        if (pos[1] > 0)
            bl = this->at(pbl) == WALL;
        if (pos[1] < this->getHeight()-1)
            br = this->at(pbr) == WALL;
    } else {
        b = true;
        bl = true;
        br = true;
    }
    if (pos[1] > 0)
        l = this->at(pl) == WALL;
    else {
        l = true;
        tl = true;
        bl = true;
    }
    if (pos[1] < this->getHeight()-1)
        r = this->at(pr) == WALL;
    else {
        r = true;
        tr = true;
        br = true;
    }

    if (l && r && ((!t && bl && b && br) || (!b && tl && t && tr))) return m_h_sprite;
    else if (t && b && ((!l && tr && r && br) || (!r && tl && l && bl))) return m_v_sprite;
    else if (l && t && ((tl && !(r || br || b)) || (!tl && r && b && br && tr && bl))) return m_br_sprite;
    else if (r && t && ((tr && !(l || bl || b)) || (!tr && l && b && bl && tl && br))) return m_bl_sprite;
    else if (l && b && ((bl && !(r || tr || t)) || (!bl && r && t && tr && br && tl))) return m_tr_sprite;
    else if (r && b && ((br && !(l || tl || t)) || (!br && l && t && bl && tl && tr))) return m_tl_sprite;
    else return m_white_sprite;
}
