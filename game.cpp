#include "game.h"
#include <cmath>

Game::Game() {
    m_level = 0;
    m_gameover = false;
    m_score = 0;
}

void Game::level1() {
    m_level = 1;
    m_gameover = false;
    std::string field = "####################"
                        "#......#####.......#"
                        "#.####.#####.##.##.#"
                        "#.####.#####.##.##.#"
                        "#.####..........##.#"
                        "#.####.###.####.##.#"
                        "#......###.####....#"
                        "######..........####"
                        "######.##.#####.####"
                        "#......##.#####....#"
                        "#.##............##.#"
                        "#.##.###.##.###.##.#"
                        "#.##.###.##.###.##.#"
                        "#...........###....#"
                        "####################";
    unsigned int size[2] = {15, 20};
    m_map = Map(field, size);
}

void Game::play() {
    // Reset score
    m_score = 0;

    // Create map
    this->level1();

    // Create pacman
    unsigned int pacman_pos[2] = {1, 1};
    int pacman_dir[2] = {0, 1};
    std::string pacman_sprite = "../PacMan/data/pacmandroit.png";
    m_pacman = PacMan(pacman_pos, pacman_dir, this->getMap(), pacman_sprite);

    // Create pacgommes
    m_pacgommes = std::vector<Entity>();
    for (unsigned int i = 0; i < m_map.getWidth(); i++) {
        for (unsigned int j = 0; j < m_map.getHeight(); j++) {
            unsigned int pos[2] = {i, j};
            if (m_map.at(pos) == WAY)
                m_pacgommes.push_back(Entity(pos, this->getMap(), "../PacMan/data/pacgomme.png"));
        }
    }

    // Create ghosts
    m_ghosts = std::vector<Ghost>();
    unsigned int pos[2] = {0, 0};
    int dir[2] = {0, 0};
    bool incorrectPos = true;
    unsigned int* size = this->getMap()->getSize();
    for (unsigned int i = 0; i < 3; i++) {
        int d = rand()%2;
        dir[d] = (rand()%2)*2-1;
        do {
            pos[0] = rand()%size[0];
            pos[1] = rand()%size[1];
            incorrectPos = this->getMap()->at(pos) == WALL || sqrt(pow(pacman_pos[0]-pos[0], 2) + pow(pacman_pos[1]-pos[1], 2)) < 10;
        } while(incorrectPos);
        m_ghosts.push_back(Ghost(pos, dir, this->getMap(), "../PacMan/data/fantome.png"));
    }
}

void Game::nextTurn() {
    m_pacman.move();
    for (unsigned int i = 0; i < m_ghosts.size(); i++)
        m_ghosts[i].move();
    this->eatPacgommes();
    this->interactionPacmanGhosts();
}

void Game::eatPacgommes() {
    unsigned int pacman_pos[2] = {m_pacman.getPos()[0], m_pacman.getPos()[1]};
    unsigned int pacgomme_pos[2] = {0, 0};
    for (unsigned int i = 0; i < m_pacgommes.size(); i++) {
        pacgomme_pos[0] = m_pacgommes[i].getX();
        pacgomme_pos[1] = m_pacgommes[i].getY();
        if (pacman_pos[0] == pacgomme_pos[0] && pacman_pos[1] == pacgomme_pos[1]) {
            m_pacgommes.erase(m_pacgommes.begin() + i);
            m_score += 10;
            emit scoreChanged(m_score);
            if (m_pacgommes.empty()) {
                m_level++;
                emit nextLevel();
            }
            return;
        }
    }
}

void Game::interactionPacmanGhosts() {
    unsigned int* pacman_pos = m_pacman.getPos();
    for (unsigned int i = 0; i < m_ghosts.size(); i++) {
        unsigned int* ghost_pos = m_ghosts[i].getPos();
        if (pacman_pos[0] == ghost_pos[0] && pacman_pos[1] == ghost_pos[1]) {
            m_gameover = true;
            return;
        }
    }
}
