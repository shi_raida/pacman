#include <stdexcept>
#include <iostream>
#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), m_ui(new Ui::MainWindow){
    m_ui->setupUi(this);
    try {
        this->loadSprites();
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    this->setFixedSize(m_bkgnd_sprite.size());
    this->setButtons();

    m_score_label = new QLabel("Score: 0",this);
    m_score_label->setGeometry(525,676,200,50);
    connect(&m_game, &Game::scoreChanged, this, &MainWindow::updateScore);

    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(nextTurn()));
    connect(&m_game, &Game::nextLevel, this, &MainWindow::play);

    update();
}

MainWindow::~MainWindow()
{
    delete m_ui;
}

void MainWindow::loadSprites() {
    m_bkgnd_sprite = QPixmap("../PacMan/data/background.png");
    m_home_sprite = QPixmap("../PacMan/data/backgroundhome.png");
    m_classement_sprite = QIcon("../PacMan/data/classement.png");
    m_ftm_m_sprite = QIcon("../PacMan/data/fantome-.png");
    m_ftm_a_sprite = QIcon("../PacMan/data/fantome+.png");
    m_gameover_sprite = QPixmap("../PacMan/data/gameover.png");
    m_info_sprite = QIcon("../PacMan/data/information.png");
    m_play_sprite = QIcon("../PacMan/data/play.png");
    this->checkSprites();
}

void MainWindow::checkSprites() {
    if (m_bkgnd_sprite.isNull())
        throw std::runtime_error("'background.png' is not loaded.");
    if (m_home_sprite.isNull())
        throw std::runtime_error("'backgroundhome.png' is not loaded.");
    if (m_classement_sprite.isNull())
        throw std::runtime_error("'classement.png' is not loaded.");
    if (m_ftm_m_sprite.isNull())
        throw std::runtime_error("'fantome-.png' is not loaded.");
    if (m_ftm_a_sprite.isNull())
        throw std::runtime_error("'fantome+.png' is not loaded.");
    if (m_gameover_sprite.isNull())
        throw std::runtime_error("'gameover.png' is not loaded.");
    if (m_info_sprite.isNull())
        throw std::runtime_error("'information.png' is not loaded.");
    if (m_play_sprite.isNull())
        throw std::runtime_error("'play.png' is not loaded.");
}

void MainWindow::setButtons() {
    m_play_btn = new QPushButton("", this);
    m_play_btn->setGeometry(QRect(QPoint(530, 550), QSize(120, 120)));
    m_play_btn->setStyleSheet("border: 0px;");
    m_play_btn->setIcon(m_play_sprite);
    m_play_btn->setIconSize(QSize(100, 100));
    connect(m_play_btn, SIGNAL(clicked()), this, SLOT(play()));
}

void MainWindow::updateScore(int score) {
    m_score_label->setText("Score: " + QString::number(score));
}

void MainWindow::play() {
    m_game.play();
    update();
    m_timer->start(250);
}

void MainWindow::nextTurn() {
    m_game.nextTurn();
    if (m_game.isGameover())
        m_timer->stop();
    update();
}

void MainWindow::paintEvent(QPaintEvent*) {
    QPainter painter(this);
    QPalette palette;

    // Background
    if (m_game.getLevel() == 0)
        palette.setBrush(backgroundRole(), QBrush(m_home_sprite));
    else if (!m_game.isGameover())
        palette.setBrush(backgroundRole(), QBrush(m_bkgnd_sprite));
    else {
        palette.setBrush(backgroundRole(), QBrush(m_gameover_sprite));
        this->setPalette(palette);
        return;
    }


    // Map
    if (m_game.getLevel() != 0) {
        Map* map = this->getGame()->getMap();
        unsigned int pos[2] = {0, 0};

        m_tile_width = map->getSprite(pos).width();
        m_tile_height = map->getSprite(pos).height();

        for (pos[0] = 0; pos[0] < map->getWidth(); pos[0]++) {
            for (pos[1] = 0; pos[1] < map->getHeight(); pos[1]++) {
                painter.drawPixmap((pos[1]+1)*m_tile_width, (pos[0]+2)*m_tile_height, map->getSprite(pos));
            }
        }
    }

    // PacMan
    MovingEntity *pacman = m_game.getPacman();
    painter.drawPixmap((pacman->getY()+1)*m_tile_width, (pacman->getX()+2)*m_tile_height, pacman->getSprite());

    // PacGommes
    std::vector<Entity> pacgommes = m_game.getPacGommes();
    for (unsigned int i = 0; i < pacgommes.size(); i++)
        painter.drawPixmap((pacgommes[i].getY()+1)*m_tile_width, (pacgommes[i].getX()+2)*m_tile_height, pacgommes[i].getSprite());

    // Ghosts
    std::vector<Ghost> ghosts = m_game.getGhosts();
    for (unsigned int i = 0; i < ghosts.size(); i++)
        painter.drawPixmap((ghosts[i].getY()+1)*m_tile_width, (ghosts[i].getX()+2)*m_tile_height, ghosts[i].getSprite());

    this->setPalette(palette);
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    // PacMan control
    int dir[2] = {0, 0};
    if (event->key() == Qt::Key_Z)
        dir[0] = -1;
    else if (event->key() == Qt::Key_D)
        dir[1] = 1;
    else if (event->key() == Qt::Key_S)
        dir[0] = 1;
    else if (event->key() == Qt::Key_Q)
        dir[1] = -1;
    m_game.getPacman()->setDir(dir);
}

void QPushButton::keyPressEvent (QKeyEvent *event)
{
    if (parent() != NULL)
        QCoreApplication::sendEvent(parent(),event);
}
