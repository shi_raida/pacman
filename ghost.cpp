#include "ghost.h"


Ghost::Ghost(): MovingEntity() {}

Ghost::Ghost(const unsigned int pos[2], const int dir[2], Map* map, std::string sprite): MovingEntity(pos, dir, map, sprite) {}

void Ghost::move() {
    unsigned int old_pos[2] = {m_pos[0], m_pos[1]};
    MovingEntity::move();
    if (m_pos[0] == old_pos[0] && m_pos[1] == old_pos[1]) {
        m_dir[0] = 0;
        m_dir[1] = 0;
        int dir = rand()%2;
        m_dir[dir] = (rand()%2)*2-1;
        MovingEntity::move();
    }
}
