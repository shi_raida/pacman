#include <cmath>
#include "movingentity.h"


MovingEntity::MovingEntity() {}

MovingEntity::MovingEntity(const unsigned int pos[2], const int dir[2], Map* map, std::string sprite): Entity(pos, map, sprite) {
    this->setDir(dir);
}

void MovingEntity::setDir(const int dir[2]) {
    this->setHDir(dir[0]);
    this->setVDir(dir[1]);
}

void MovingEntity::setHDir(const int dir) {
    if (dir == 0)
        m_dir[0] = 0;
    else
        m_dir[0] = dir/std::abs(dir);
}

void MovingEntity::setVDir(const int dir) {
    if (dir == 0)
        m_dir[1] = 0;
    else
        m_dir[1] = dir/std::abs(dir);
}

void MovingEntity::move() {
    unsigned int new_pos[2] = {this->getX()+this->getHDir(), this->getY()+this->getVDir()};
    if (this->getMap()->at(new_pos) != WALL)
        this->setPos(new_pos);
}
