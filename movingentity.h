#ifndef MOVINGENTITY_H
#define MOVINGENTITY_H

#include "entity.h"


class MovingEntity : public Entity
{
public:
    // Constructors
    MovingEntity();
    MovingEntity(const unsigned int pos[2], const int dir[2], Map* map, std::string sprite);

    // Getters
    inline int* getDir() {return m_dir;}
    inline int getHDir() {return m_dir[0];}
    inline int getVDir() {return m_dir[1];}

    // Setters
    virtual void setDir(const int dir[2]);
    void setHDir(const int dir);
    void setVDir(const int dir);

    // Others
    virtual void move();

protected:
    int m_dir[2];   // [dir_x, dir_y];
};

#endif // MOVINGENTITY_H
