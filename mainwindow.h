#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QPainter>
#include <QKeyEvent>
#include <QTimer>
#include <QLabel>
#include "game.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    // Constructor & Destructor
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    // Getters
    inline Game* getGame() {return &m_game;}

protected:
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);

private slots:
    // Actions
    void play();
    void nextTurn();

private:
    // Setters
    void setButtons();

    // Others
    void loadSprites();
    void checkSprites();
    void updateScore(int score);


    Ui::MainWindow *m_ui;
    Game m_game;
    QTimer* m_timer;

    // Sprites
    unsigned int m_tile_width;
    unsigned int m_tile_height;
    QPixmap m_bkgnd_sprite;    // Background
    QPixmap m_home_sprite;
    QIcon m_classement_sprite;
    QIcon m_ftm_m_sprite;    // Minus
    QIcon m_ftm_a_sprite;    // Add
    QPixmap m_gameover_sprite;
    QIcon m_info_sprite;
    QIcon m_play_sprite;

    // Buttons
    QLabel* m_score_label;
    QPushButton* m_play_btn;
};
#endif // MAINWINDOW_H
