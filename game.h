#ifndef GAME_H
#define GAME_H

#include <vector>
#include "map.h"
#include "pacman.h"
#include "ghost.h"


class Game: public QObject
{
    Q_OBJECT

public:
    // Constructor
    Game();

    // Getters
    inline unsigned int getLevel() {return m_level;}
    inline bool isGameover() {return m_gameover;}
    inline Map* getMap() {return &m_map;}
    inline MovingEntity* getPacman() {return &m_pacman;}
    inline std::vector<Entity>& getPacGommes() {return m_pacgommes;}
    inline int getScore() {return m_score;}
    inline std::vector<Ghost>& getGhosts() {return m_ghosts;}

    // Levels
    void level1();

    // Others
    void play();
    void nextTurn();
    void eatPacgommes();
    void interactionPacmanGhosts();

signals:
    void scoreChanged(int score);
    void nextLevel();

private:
    unsigned int m_level;
    bool m_gameover;
    Map m_map;
    PacMan m_pacman;
    std::vector<Entity> m_pacgommes;
    unsigned int m_score;
    std::vector<Ghost> m_ghosts;
};

#endif // GAME_H
