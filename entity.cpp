#include <stdexcept>
#include "entity.h"


Entity::Entity() {}

Entity::Entity(const unsigned int pos[2], Map* map, std::string sprite) {
    this->setMap(map);
    this->setPos(pos);
    this->loadSprite(sprite);
}

void Entity::setPos(const unsigned int pos[2]) {
    if (pos[0] >= this->getMap()->getWidth())
        throw std::invalid_argument("Entity::setPos() : x must be in the map width.");
    if (pos[1] >= this->getMap()->getHeight())
        throw std::invalid_argument("Entity::setPos() : y must be in the map height.");
    else if (this->getMap()->at(pos) == WALL)
        throw std::invalid_argument("Entity::setPos() : Trying to set an entity in a WALL.");
    else {
        m_pos[0] = pos[0];
        m_pos[1] = pos[1];
    }
}

void Entity::setX(const unsigned int x) {
    unsigned int new_pos[2] = {x, this->getY()};
    if (x >= this->getMap()->getWidth())
        throw std::invalid_argument("Entity::setX() : x must be in the map width.");
    else if (this->getMap()->at(new_pos) == WALL)
        throw std::invalid_argument("Entity::setX() : Trying to set an entity in a WALL.");
    else
        m_pos[0] = x;
}

void Entity::setY(const unsigned int y) {
    unsigned int new_pos[2] = {this->getX(), y};
    if (y >= this->getMap()->getHeight())
        throw std::invalid_argument("Entity::setY() : y must be in the map height.");
    else if (this->getMap()->at(new_pos) == WALL)
        throw std::invalid_argument("Entity::setY() : Trying to set an entity in a WALL.");
    else
        m_pos[1] = y;
}

void Entity::loadSprite(std::string sprite) {
    m_sprite = QPixmap(sprite.c_str());
    if (m_sprite.isNull()) {
        std::string s("Impossible to load '" + sprite + "'.");
        throw std::runtime_error(s);
    }
}

void Entity::loadSprite(QPixmap& sprite) {
    m_sprite = sprite;
}
