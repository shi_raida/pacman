#ifndef GHOST_H
#define GHOST_H

#include "movingentity.h"


class Ghost : public MovingEntity
{
public:
    // Constructors
    Ghost();
    Ghost(const unsigned int pos[2], const int dir[2], Map* map, std::string sprite);

public:
    virtual void move();
};

#endif // GHOST_H
