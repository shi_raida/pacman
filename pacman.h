#ifndef PACMAN_H
#define PACMAN_H

#include "movingentity.h"


class PacMan : public MovingEntity
{
public:
    // Constructors
    PacMan();
    PacMan(const unsigned int pos[2], const int dir[2], Map* map, std::string sprite);

    // Setter
    virtual void setDir(const int dir[2]);

private:
    void loadSprites();


    QPixmap m_t_sprite;
    QPixmap m_r_sprite;
    QPixmap m_b_sprite;
    QPixmap m_l_sprite;
};

#endif // PACMAN_H
