#ifndef ENTITY_H
#define ENTITY_H

#include <QPixmap>
#include "map.h"

class Entity
{
public:
    // Constructors
    Entity();
    Entity(const unsigned int pos[2], Map* map, std::string sprite);

    // Getters
    inline unsigned int* getPos() {return m_pos;}
    inline unsigned int getX() {return m_pos[0];}
    inline unsigned int getY() {return m_pos[1];}
    inline Map* getMap() {return m_map;}
    inline QPixmap& getSprite() {return m_sprite;}

    // Setters
    void setPos(const unsigned int pos[2]);
    void setX(const unsigned int x);
    void setY(const unsigned int y);
    inline void setMap(Map* map) {m_map = map;}
    void loadSprite(std::string sprite);
    void loadSprite(QPixmap& sprite);

protected:
    unsigned int m_pos[2];  // [x, y]
    Map* m_map;
    QPixmap m_sprite;
};

#endif // ENTITY_H
